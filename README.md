# Exercice 1 Abdelrrahman


## Inputs

1 - Nom de la VM <br/>
2 - Subnet de destination<br/>
3 - SKU de la VM<br/>
4 - OS<br/>
5 - DataCenter<br/>

## Ce que fait le pipeline
Utiliser Azure CLI à l'intérieur du pipeline.<br/>
1 - Créer un VNET si non existant<br/>
2 - Créer un subnet si non existant<br/>
3 - Créer une VM avec le nom demandé si non déjà existante<br/>
Essayer de découper le pipeline en plein de petit job.<br/>
1 job = 1 action bien spécifique.<br/>
Essayer de paralléliser ce qui est parallélisable.<br/>

## Outputs

Au format JSON<br/>
1 - ID Azure de la VM<br/>
2 - Adresse IP<br/>
3 - Login (autogénéré)<br/>
4 - Password (autogénéré)<br/>


